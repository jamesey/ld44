extends AnimatedSprite

export var max_random_scale := 4

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("animation_finished", self, "kill")
	scale *= rand_range(1, max_random_scale)
	frame = 0
	playing = true
	Engine.time_scale *= rand_range(0.0001, 0.4)
	if is_instance_valid(GameManager.player_camera):
		GameManager.player_camera.shake_screen(3, 0.25)

func kill():
	if !visible:
		return
	visible = false
	yield(get_tree().create_timer(3), "timeout")
	queue_free()
	