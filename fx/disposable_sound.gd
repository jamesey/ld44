extends AudioStreamPlayer2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	#get_parent().remove_child(self)
	#get_tree().get_root().add_child(self)
	connect("finished", self, "kill")
	pitch_scale = rand_range(0.9, 1.05)
	play()

func kill():
	queue_free()