extends Control

func _input(event):
	if Input.is_action_just_pressed("start"):
		get_tree().change_scene(GameManager.current_level_path)
		Input.action_release("start")

func _physics_process(delta):
	Engine.time_scale = 1