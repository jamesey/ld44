extends Node2D

export var scene_path := "res://scn/Level2.tscn"
func _ready():
	if GameManager.pending_1 and name == "Tutorial":
		DialogueUI.show_dialogue("1")
		GameManager.pending_1 = false
		
# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	GameManager.enemies_to_kill = $Enemies.get_child_count()
	GameManager.current_level_path = filename
	
	if GameManager.enemies_to_kill <= 0:
		SceneSwapper.swap_scene(scene_path)
	
	if GameManager.enemies_to_kill <= 2 and name == "Level2":
		for enemy in $Enemies.get_children():
			enemy.always_aggro = true
	
	if name == "BossFight" and !SpecialSong.is_playing():
		SpecialSong.play()
