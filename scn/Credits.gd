extends Node2D
onready var c : Control = $CanvasLayer/Control
onready var anim : AnimationPlayer = $AnimationPlayer
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pause_mode = Node.PAUSE_MODE_STOP
	get_tree().paused = true
	yield(get_tree().create_timer(20), "timeout")
	get_tree().paused = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	c.visible = false
	c.visible = true
	anim.set_speed_scale(1 / Engine.time_scale)
	
	var money_str : String = PlayerHUD.get_money_str(GameManager.total_money + GameManager.player.money)
	$CanvasLayer/Control/Label.text = "Income: " + money_str
	
