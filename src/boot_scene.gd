extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	$BlackCover.visible = false
	if OS.get_name() == "HTML5":
		return
	$BlackCover.visible = true
	yield(get_tree().create_timer(0), "timeout")
	go_to_title()


func _input(event):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		$BlackCover.visible = true
		yield(get_tree().create_timer(0.5), "timeout")
		go_to_title()

func go_to_title():
	get_tree().change_scene("res://scn/TitleScreen.tscn")