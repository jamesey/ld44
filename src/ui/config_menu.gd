class_name ConfigMenu
extends GridContainer

onready var config_control : Control = owner.get_node("MenuLayer/Config")

var default_config_values : Dictionary = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	# Get default values and connect signals
	for element in get_children():
		if element is Range:
			default_config_values[element.name] = element.value
			element.connect("value_changed", self, "on_range_value_changed", [element])
		elif element is CheckBox:
			default_config_values[element.name] = element.pressed
			element.connect("toggled", self, "on_checkbox_value_changed", [element])

	# Set values that could have been changed externally
	System.config_dict["FullscreenCheckBox"] = OS.window_fullscreen

	# Load values from config dict and apply actions
	for key in System.config_dict.keys():
		var value = System.config_dict[key]
		if !has_node(key):
			continue
		var element : Control = get_node(key)
		if element is Range:
			element.value = value
			on_range_value_changed(value, element)
		elif element is CheckBox:
			element.pressed = value
			on_checkbox_value_changed(value, element)

	config_control.connect("visibility_changed", self, "on_visibility_changed")

func on_visibility_changed():
	if config_control.visible:
		# Update values that might have changed somewhere else
		$FullscreenCheckBox.pressed = OS.window_fullscreen
		if get_focus_owner() == null or get_focus_owner().get_parent() != self:
			$FullscreenCheckBox.grab_focus()
	else:
		System.save_config()


func on_range_value_changed(value : float, element : Range):
	# Update config dict
	System.config_dict[element.name] = value
	# Apply value
	apply_value_action(element.name, value)

func on_checkbox_value_changed(pressed : bool, element : CheckBox):
	# Update config dict
	System.config_dict[element.name] = pressed
	# Apply value
	apply_value_action(element.name, pressed)

static func apply_value_action(value_name : String, value):
	match value_name:
		"MasterSoundHSlider":
			set_audio_bus_volume("Master", value)
		"InterfaceSoundHSlider":
			set_audio_bus_volume("Interface", value)
		"MusicSoundHSlider":
			set_audio_bus_volume("Music", value)
		"SFXSoundHSlider":
			set_audio_bus_volume("SFX", value)
		"VsyncCheckBox":
			OS.vsync_enabled = value
		"FullscreenCheckBox":
			OS.window_fullscreen = value
		_:
			push_warning("Action not implemented: " + value_name)


static func set_audio_bus_volume(bus_name : String, value : float, min_cutoff : float = -34.9):
	if value <= min_cutoff:
		value = -9999
	var bus_idx : int = AudioServer.get_bus_index(bus_name)
	AudioServer.set_bus_volume_db(bus_idx, value)