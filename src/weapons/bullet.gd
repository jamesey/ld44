class_name Bullet
extends Area2D

onready var f : Node2D = $Flash

export var is_template := false
var velocity := Vector2()
var life_seconds := 5.0
var damage := 1.0
var knockback := 7.0
var rumble_power := 1.0
var owning_body : KinematicBody2D = null
var ignore_list := []
var ignores_enemies := false
var no_stop := false

# Called when the node enters the scene tree for the first time.
func _ready():
	if is_template:
		visible = false
		set_physics_process(false)
		return
	rotation = rand_range(0, 1)
	connect("body_entered", self, "on_body_entered")

func _physics_process(delta):
	if is_template:
		return
		
	position += velocity * delta
	life_seconds -= delta
	if life_seconds <= 0:
		queue_free()
	if life_seconds <= 4.999:
		f.visible = false

func on_body_entered(body : PhysicsBody2D):
	if !visible:
		queue_free()
		return
	
	if body is Enemy and ignores_enemies:
		return

	for item in ignore_list:
		if body == item:
			return
			
	# Apply damage
	if body is Player or body is Enemy:
		body.damage(damage)
		var knockback_vector := (body.global_position - global_position).normalized()
		body.apply_simple_impulse(knockback * knockback_vector)
	
	if body is Enemy:
		if body.attack_other_enemies_if_hit or owning_body is Player:
			if is_instance_valid(owning_body) and (owning_body is Player or owning_body is Enemy):
				body.player_ref = owning_body
				
				
	elif body is Player:
		if !GameManager.player.keyboard_control:
			Input.start_joy_vibration(0, rumble_power, rumble_power, 0.25)
		GameManager.player_camera.shake_screen(10, 0.25)
	
	if !no_stop:
		visible = false
		queue_free()
	
