extends Node2D
var next_scene : PackedScene = preload("res://scn/StartScene.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Apply a few config values on startup (e.g. sound)
	var value_names : Array = ["MasterSoundHSlider", "MusicSoundHSlider", "VsyncCheckBox", "FullscreenCheckBox"]
	for value_name in value_names:
		if !System.config_dict.has(value_name):
			continue
		ConfigMenu.apply_value_action(value_name, System.config_dict[value_name])

func _input(event: InputEvent) -> void:
	if event is InputEventAction or event is InputEventKey or Input.is_action_just_released("next_dialogue") or Input.is_mouse_button_pressed(BUTTON_LEFT):
		if OS.get_ticks_msec() >= 4000 and $AnimationPlayer.current_animation != "exit":
			$AnimationPlayer.play("exit")

			yield(get_tree().create_timer(2.0), "timeout")
			get_tree().change_scene_to(next_scene)


