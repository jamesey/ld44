extends Tween
# Simple tweener (will add more advanced functionality with multiple tracks later)

func _ready():
	start()

func add_motion_tween(target : Node, start_pos, end_pos, duration := 1.0, transition := Tween.TRANS_LINEAR, ease_type := Tween.EASE_IN):
	return interpolate_property(target, "position",
		start_pos, end_pos, duration,
		transition, ease_type)

func add_tween(target : Object, property: String, start_pos, end_pos, duration := 1.0, transition := Tween.TRANS_LINEAR, ease_type := Tween.EASE_IN):
	return interpolate_property(target, property,
		start_pos, end_pos, duration,
		transition, ease_type)

func remove(object : Object, key := ""):
	return .remove(object, key)

func remove_all():
	return .remove_all()