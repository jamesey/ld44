extends Area2D

onready var shape : CollisionShape2D = $CollisionShape2D
onready var ray : RayCast2D = owner.get_node("StaticBodyStyleStopperRaycast")

# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	var give_money := true
	if ray.is_colliding():
		give_money = false
	
	var overlapping_areas := get_overlapping_areas()
	for area in overlapping_areas:
		if area is Bullet:
			# If the bullet's owner is not the player
			if area.owning_body != owner:
				var closeness : float = shape.shape.radius - area.global_position.distance_to(global_position)
				closeness = clamp(closeness, 0, 100)
				closeness *= 0.00005 * delta * 120
				# If slo-mo is activated, get double money
				if Engine.time_scale <= 0.5:
					closeness *= 2
				closeness *= area.damage
				if give_money and ray.cast_to != Vector2.ZERO:
					owner.money += closeness
				ray.cast_to = (area.global_position - global_position).rotated(-owner.rotation)
				ray.enabled = true
				