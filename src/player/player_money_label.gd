extends Label

var offset := Vector2(-20, -40)
var money_label : Label = null

# Called when the node enters the scene tree for the first time.
func _ready():
	money_label = owner.get_node("PlayerHUD/MoneyLabel")


func _physics_process(delta):
	text = money_label.text
	var player_transform : Transform2D = owner.get_global_transform_with_canvas()
	rect_position = player_transform.origin + offset
