class_name Player
extends KinematicBody2D

const LEFT_STICK_AXIS_DEADZONE := 0.1
const LEFT_STICK_DEADZONE := 0.4
const LEFT_STICK_FULLZONE := 0.95

var cost : Dictionary = {
	"bullets" : 0.01,
	"dodging" : 0.17,
	"getting hit" : 0.99,
	"kills" : 0.20
}

var health := 10.0
var money := 0.00
var max_money := 10.00

export var can_die := true

# Tweakers
var acceleration_spd := 0.35
var max_velocity := 2.5
var max_spd_lerp_ramp := 0.1
var release_damping := 0.05

var rot_spd := 0.4

var gravity_vector := 0 * Vector2.DOWN # Used for either platformers or "wind" effects
var wall_slide_friction := 0.82 # Used primarily for top down games - set to 1.0 to disable

var impulse_max_velocity_offset_damping := 0.2 # Should be close to acceleration speed

var input_vector := Vector2()
var input_magnitude := 0.0

var velocity := Vector2()

var keyboard_control := false

var max_spd_mod := 1.0
var dodge_active := false

var impulse_max_velocity_offset := Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.player = self
	money = 3.00

func _input(event):
	if event is InputEventKey:
		keyboard_control = true
	elif event is InputEventMouseButton:
		keyboard_control = true
	elif event is InputEventMouseMotion:
		keyboard_control = true
	elif event is InputEventJoypadButton:
		keyboard_control = false
	elif event is InputEventJoypadMotion:
		if abs(event.axis_value) > 0.3:
			keyboard_control = false
	

func _physics_process(delta):
	if Input.is_action_pressed("suicide"):
		money = -1
		damage(1)
	var update_rotation := false
	var rot_dir := get_global_mouse_position() - global_position
	
	if keyboard_control:
		update_rotation = true
	var gp_rot_dir := Vector2()
	
	gp_rot_dir.x = Input.get_action_strength("right_stick_right") - Input.get_action_strength("right_stick_left")
	gp_rot_dir.y = Input.get_action_strength("right_stick_down") - Input.get_action_strength("right_stick_up")
	
	if gp_rot_dir.length() > 0.2:
		rot_dir = gp_rot_dir
		update_rotation = true
	
	if update_rotation:
		var prev_rot_dir := Vector2.RIGHT.rotated(rotation)
		rot_dir = prev_rot_dir.linear_interpolate(rot_dir, rot_spd)
		rotation = rot_dir.angle()
	
	# Get input vector and input magnitude (only relevant for controllers)
	input_vector.x = Input.get_action_strength("right") - Input.get_action_strength("left")
	input_vector.y = Input.get_action_strength("down") - Input.get_action_strength("up")
	if abs(input_vector.x) < LEFT_STICK_AXIS_DEADZONE:
		input_vector.x = 0
	if abs(input_vector.y) < LEFT_STICK_AXIS_DEADZONE:
		input_vector.y = 0
	input_magnitude = clamp(input_vector.length_squared(), 0, 1)
	
	if input_magnitude > LEFT_STICK_FULLZONE:
		input_magnitude = 1
	elif input_magnitude < LEFT_STICK_DEADZONE * LEFT_STICK_DEADZONE:
		input_magnitude = 0
	
	if Input.is_action_pressed("slow_walk"):
		input_magnitude *= 0.4
		
	input_vector = input_magnitude * input_vector.normalized()
	
	var max_spd_mod_target = clamp(input_magnitude, 0.1, 1.0)
	var spd_mod_lerp_spd := 0.05
	# Lerp faster if increasing
	if max_spd_mod_target > max_spd_mod:
		spd_mod_lerp_spd = max_spd_lerp_ramp
	
	max_spd_mod = lerp(max_spd_mod, max_spd_mod_target, spd_mod_lerp_spd)
	if max_spd_mod > 0.95 and max_spd_mod_target > max_spd_mod:
		max_spd_mod = 1
	
	
	var acceleration := acceleration_spd * input_vector
	velocity += input_magnitude * acceleration
	
	# Clamp velocity to never exceed limit 
	var velocity_magnitude : float = clamp(velocity.length(), 0, max_spd_mod * max_velocity + impulse_max_velocity_offset.length())

	# Damp velocity on release (zero input vector)
	if input_magnitude <= 0:
		velocity_magnitude -= release_damping 
		if velocity_magnitude < 0:
			velocity_magnitude = 0
	
	# Damp impulse offset
	var impulse_max_velocity_offset_magnitude := impulse_max_velocity_offset.length()
	impulse_max_velocity_offset_magnitude -= impulse_max_velocity_offset_damping
	if impulse_max_velocity_offset_magnitude < 0:
		impulse_max_velocity_offset_magnitude = 0
	impulse_max_velocity_offset = impulse_max_velocity_offset_magnitude * impulse_max_velocity_offset.normalized()
	
	velocity = velocity_magnitude * velocity.normalized()
	
	var final_rot_vec := Vector2.RIGHT.rotated(rotation)
	# Dash move
	if Input.is_action_just_pressed("dash") and money >= cost["dodging"]:
		var dash_dir := Vector2.ZERO
		if input_vector.length() > 0:
			dash_dir = input_vector.normalized()
		elif final_rot_vec.length() > 0:
			dash_dir = final_rot_vec
		apply_simple_impulse(4 * dash_dir)
		dodge_active = true
		money -= cost["dodging"]
		# TODO : use facing direction as last resort
	
	# Apply gravity
	velocity += gravity_vector
	

	# Finally move and check for collisions
	var collision := move_and_collide(velocity * delta * 120)
	# Slide along collision normal if colliding
	if collision:
		velocity = wall_slide_friction * velocity.slide(collision.normal)
		# Clamp velocity again to be safe
		velocity = velocity.clamped(max_velocity + impulse_max_velocity_offset.length())
		move_and_collide(velocity * delta * 120)

	money = clamp(money, 0, max_money)

# Applies a simple impulse to the character.
func apply_simple_impulse(impulse_vector : Vector2, clear_current_velocity := true, break_velocity_limit := true):
	if clear_current_velocity:
		velocity = Vector2()
	
	velocity += impulse_vector
	
	impulse_max_velocity_offset += impulse_vector
	
func damage(amount : float):
	# If you get damaged without money, game over.
	if money <= 0:
		game_over()
	money -= cost["getting hit"]
	if money < 0:
		game_over()
	
func game_over():
	if !can_die:
		return
	get_tree().change_scene("res://scn/GameOver.tscn")
	print("GAME OVER")
	