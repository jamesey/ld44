extends Area2D


func _physics_process(delta):
	var overlapping_areas := get_overlapping_areas()
	var has_bullet := false
	for area in overlapping_areas:
		if area is Bullet and area.owning_body != owner:
			has_bullet = true
	if has_bullet and owner.dodge_active:
		Engine.time_scale *= 0.1
	else:
		Engine.time_scale += 0.01
		owner.dodge_active = false
	AudioServer.get_bus_effect(0, 0).pitch_scale = clamp(Engine.time_scale, 0.9, 1)	
	Engine.time_scale = clamp(Engine.time_scale, 0.1, 1)