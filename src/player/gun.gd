class_name Gun
extends Node2D

onready var bullet : PackedScene = preload("res://src/weapons/bullet.tscn")

var rapid_fire := true
var cooldown_timer_time := 0.2
var bullet_spd := 1000.0
var recoil := 2.0
var rumble_power := 0.3
var shake_screen_power := 3.0
var cost := 0.01

var cooldown_timer := 0.0
var muzzle_flash_timer := 0.0
var muzzle_flash_timer_time := 0.1

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_just_pressed("shoot") or (rapid_fire and Input.is_action_pressed("shoot")):
		shoot()

func _physics_process(delta):
	if cooldown_timer >= 0:
		cooldown_timer -= delta
	if muzzle_flash_timer >= 0:
		muzzle_flash_timer -= delta
		
	if muzzle_flash_timer > 0:
		owner.get_node("MuzzleFlash").visible = true
	else:
		owner.get_node("MuzzleFlash").visible = false

func shoot():
	if cooldown_timer > 0:
		return
		
	if owner is Player and owner.money <= 0:
		return
		
	cooldown_timer = cooldown_timer_time
	
	var new_bullet := bullet.instance()
	new_bullet.global_position = global_position
	new_bullet.is_template = false
	new_bullet.velocity = bullet_spd * Vector2.RIGHT.rotated(owner.rotation)
	new_bullet.ignore_list.append(owner)
	new_bullet.owning_body = owner
	owner.owner.add_child(new_bullet)
	
	if owner is Player or owner is Enemy:
		var recoil_dir : Vector2 = (owner.global_position - new_bullet.global_position).normalized()
		owner.apply_simple_impulse(recoil * recoil_dir)
		
		if shake_screen_power > 0.001:
			GameManager.player_camera.shake_screen(shake_screen_power, 0.25, -new_bullet.velocity.normalized())
	
	$gunshot.play()
	
	if owner is Player:
		if !owner.keyboard_control:
			Input.start_joy_vibration(0, rumble_power, rumble_power, 0.25)
		owner.money -= owner.cost["bullets"]
		muzzle_flash_timer = muzzle_flash_timer_time
		
		
	