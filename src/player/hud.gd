class_name PlayerHUD
extends CanvasLayer

onready var money_label : Label = $MoneyLabel
onready var cost_label : Label = $CostLabel
onready var money_tick_sound : AudioStreamPlayer = $MoneyLabel/tick
onready var red_cover : ColorRect = $RedCover
onready var enemies_label : Label = $EnemiesLabel

var pitch_reset_timer : float = 0.00
var pitch_reset_timer_time = 1.0

func _physics_process(delta):
	enemies_label.text = str(GameManager.enemies_to_kill) + " left."
	update_cost_label()
	update_money_label()
	money_tick_sound.pitch_scale = clamp(money_tick_sound.pitch_scale, 0.2, 8)
	
	if pitch_reset_timer >= 0:
		pitch_reset_timer -= delta 
	else:
		money_tick_sound.pitch_scale = 0.75
	
var last_money : float = -1.00
var last_money_str : String = ""

func update_money_label():
	var money : float = owner.money
	if money == last_money:
		return
		
	var money_str := get_money_str(money)	
	
	last_money = money
	pitch_reset_timer = pitch_reset_timer_time
	if !last_money_str.ends_with(money_str):
		last_money_str = money_str	
		money_label.text = money_str
		money_tick_sound.play()
		money_tick_sound.pitch_scale *= 1.001#+= 0.01
	
	if money <= owner.cost["getting hit"]:
		money_label.add_color_override("font_color", Color.red)
		red_cover.visible = true
	else:
		money_label.add_color_override("font_color", Color.white)
		red_cover.visible = false	
func update_cost_label():
	cost_label.text = ""
	for key in owner.cost.keys():
		var value : float = owner.cost[key]
		var money_str := get_money_str(value)
		cost_label.text += key.to_upper() + ": " + money_str + "\n"
	
static func get_money_str(money : float) -> String:
	var dollars : int = int(money)
	var cents : int = int(100 * (money - dollars))
	var cents_str := str(cents)
	if cents_str.length() == 1:
		cents_str = "0" + cents_str
	var money_str : String = "$"
	money_str += str(dollars) + "." + cents_str
	return money_str

