extends Camera2D

var screen_shake := 0.0
var timer := 0.0
var direction_bias := Vector2(1,1)


# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(true)
	set_physics_process_internal(true)
	GameManager.player_camera = self

func _physics_process(delta):
	if timer >= 0:
		offset = Vector2(direction_bias.x * rand_range(-1, 1) * screen_shake, direction_bias.y *  rand_range(-1, 1) * screen_shake)
		offset.x = clamp(offset.x, -2 * screen_shake, 2 * screen_shake)
		offset.y = clamp(offset.x, -2 *screen_shake, 2 * screen_shake)	
		timer -= delta
	else: 
		offset = Vector2()
	
func shake_screen(amount : float, seconds : float, dir := Vector2(1,1)):
	screen_shake = amount
	timer = seconds
	direction_bias = dir
	