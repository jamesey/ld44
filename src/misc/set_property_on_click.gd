extends Button

export var node_path : NodePath = ""
export var property : String = "visible"
export var starting_value = false
export var value_on_click = true
export var value_off_click = false # Toggle behaviour
export var sound_on_click : NodePath = ""
export var sound_off_click : NodePath = ""

func _ready() -> void:
	get_node(node_path).set(property, starting_value)
	connect("toggled", self, "set_property")

func set_property(button_pressed) -> void:
	if button_pressed and sound_on_click != "":
		get_node(sound_on_click).play()

	elif !button_pressed and sound_off_click != "":
		get_node(sound_off_click).play()

	var value = value_off_click
	if button_pressed:
		value = value_on_click
	get_node(node_path).set(property, value)
