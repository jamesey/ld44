extends Node

export var property : String = "visible"
export var value = true
export var seconds_before_set : float = 1.0

func _ready() -> void:
	activate()

func activate() -> void:
	yield(get_tree().create_timer(seconds_before_set), "timeout")
	set(property, value)
