extends Control

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	visible = false
	DialogueManager.connect("dialogue_change", self, "on_dialogue_change")

func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("debug"):
		visible = !visible
		if visible:
			$IdLineEdit.grab_focus()
	
	if !visible:
		return 
	if Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_KP_ENTER):
		if (DialogueManager.get_current_id() != $IdLineEdit.text):
			DialogueManager.go_to_dialogue($IdLineEdit.text, true)
		
		if $TreeSceneLineEdit.text != "":
			var scene_path : String = "res://scn/" + $TreeSceneLineEdit.text + ".tscn"
			get_tree().change_scene(scene_path)

	
	if Input.is_action_just_released("escape"):
		visible = false

func on_dialogue_change():
	$IdLineEdit.text = DialogueManager.get_current_id()