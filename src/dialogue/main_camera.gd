extends Camera2D

func _notification(what):
	# Do weird alignment bug fix on focus in and out
	if what == MainLoop.NOTIFICATION_WM_FOCUS_IN:
		fix_alignment_bugs()
	elif what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		fix_alignment_bugs()

# Doing these things upon focus in and out seem to fix any alignment
# bugs that crop up.
func fix_alignment_bugs():
	drag_margin_h_enabled = false
	drag_margin_v_enabled = false

	var front_ui : Control = DialogueUI.get_node("FrontUI")
	var prev_front_ui_visibility := front_ui.visible
	front_ui.visible = false
	yield(get_tree().create_timer(0), "timeout")
	front_ui.visible = prev_front_ui_visibility

	current = false
	yield(get_tree().create_timer(0), "timeout")