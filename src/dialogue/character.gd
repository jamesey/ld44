extends TextureRect

onready var character_dimmer = get_parent().get_node("CharacterDimmer")

const CHAR_DIR : String = "res://spr/dialogue/characters/"

const DIM_SPD : float = 2.0

var char_fade_pause_timer_time : float = 0.5
var char_fade_pause_timer : float = 0

var default_char_fade_in_spd : float = 0.75
var char_fade_in_spd : float = default_char_fade_in_spd
var char_fade_out_spd : float = 1.0

var char_fade_dir : int = 0

var char_dict : Dictionary = {}
var pending_char_texture : Texture = null

func _ready():
	# Load all character images into memory
	# Get file names of all images in folder
	var char_file_names : Array = Utils.list_files_in_directory(CHAR_DIR)

	# Note: you can't list files in a directory on release builds for some stupid fudging reason.
	if OS.is_debug_build() and System.build_cache:
		System.directory_contents_cache["char_file_names"] = char_file_names
	else:
		char_file_names = System.directory_contents_cache["char_file_names"]

	for file_name in char_file_names:
		if file_name.ends_with("import"):
			continue
		var img_resource : Texture = load(CHAR_DIR + file_name)
		char_dict[file_name] = img_resource

	texture = null
	yield(get_tree().create_timer(0), "timeout")
	update_character()
	char_fade_dir = 1
	texture = pending_char_texture
	DialogueManager.connect("dialogue_change", self, "update_character")

func _process(delta):
	if char_fade_pause_timer > 0:
		char_fade_pause_timer -= delta
		return
	if char_fade_dir == 1:
		modulate.a += char_fade_in_spd * delta
	elif char_fade_dir == -1:
		modulate.a -= char_fade_out_spd * delta
		if modulate.a <= 0:
			texture = pending_char_texture
			char_fade_dir = 1
			char_fade_pause_timer = char_fade_pause_timer_time

	modulate.a = clamp(modulate.a, 0, 1)

	# Dim character layer if monologue/descriptive text (-)
	if DialogueManager.get_current_character() == "-":
		character_dimmer.modulate.a += DIM_SPD * delta
	else:
		character_dimmer.modulate.a -= DIM_SPD * delta
		
	character_dimmer.modulate.a = clamp(character_dimmer.modulate.a, 0, 1)
	
	# Disabled for LD44
	visible = false
	
func update_character():
	var extra_data : Dictionary = DialogueManager.get_current_extra_data()
	var char_name : String
	# If a different portrait is specified, use that.
	if extra_data.has("Character Portrait"):
		char_name = extra_data["Character Portrait"].strip_edges()
	else:
		char_name =  DialogueManager.get_current_character().strip_edges()

	var char_icon : TextureRect = DialogueUI.dialogue_box.get_node("CharacterIcon")
	if char_name in ["", "-"]:
		char_icon.texture = null
		return

	var new_texture : Texture
	if char_name.to_lower() == "null":
		new_texture = null
	else:
		var key : String = char_name.to_lower() + ".png"
		if !char_dict.has(key):
			return # Do nothing if portrait doesn't exist
		new_texture = char_dict[key]

	if new_texture == char_icon.texture:
		return
	char_icon.texture = new_texture
	# Get custom fade speed
	char_fade_in_spd = default_char_fade_in_spd

	if extra_data.has("Fade In Speed"):
		var value = extra_data["Fade In Speed"]
		char_fade_in_spd = float(value)

	char_fade_dir = -1
	pending_char_texture = new_texture

