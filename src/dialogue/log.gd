extends RichTextLabel

func _ready() -> void:
	DialogueManager.connect("dialogue_change", self, "update_log")

func _process(delta: float) -> void:
	if !get_parent().visible:
		return

	var scroll_amt : float = Input.get_action_strength("down_axis") - Input.get_action_strength("up_axis")
	var button_scroll_amt : float = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	if scroll_amt == 0:
		scroll_amt = button_scroll_amt

	if scroll_amt != 0:
		var scroll_bar : VScrollBar = get_v_scroll()
		scroll_bar.value += scroll_amt * 1000 * delta

func update_log() -> void:
	# Parses bbcode, then discards tags to avoid issues.
	bbcode_enabled = true
	bbcode_text = DialogueManager.past_dialogue_str
	bbcode_text = text