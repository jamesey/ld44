extends CanvasLayer

onready var dialogue_box : DialogueBox = $FrontUI/DialogueBox
var dialogue_rich_text_label : RichTextLabel = null
var front_buttons : HBoxContainer = null

# Called when the node enters the scene tree for the first time.
func _ready():
	dialogue_box = $FrontUI/DialogueBox
	front_buttons = $FrontButtonsLayer/FrontButtons
	dialogue_rich_text_label = dialogue_box.get_node("DialogueRichTextLabel")
	
	dialogue_box.visible = false
	front_buttons.visible = false

func show_dialogue(id : String = ""):
	if id != "":
		set_visible_chars(0)
		DialogueManager.go_to_dialogue(id)
	dialogue_box.visible = true

func hide_dialogue():
	dialogue_box.visible = false

func set_text(text : String):
	dialogue_rich_text_label.text = text
	
func set_visible_chars(visible_chars : int):
	dialogue_box.visible_characters = visible_chars
	dialogue_rich_text_label.visible_characters = visible_chars

func is_dialogue_box_visible() -> bool:
	return dialogue_box.visible