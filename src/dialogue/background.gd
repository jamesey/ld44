extends TextureRect

const BG_DIR : String = "res://spr/dialogue/backgrounds/"

onready var previous_bg : TextureRect = get_parent().get_node("PreviousBG")

var default_bg_fade_spd : float = 0.5
var bg_fade_spd : float = default_bg_fade_spd

var bg_dict : Dictionary = {}

func _ready():
	# Load all background images into memory
	# Get file names of all images in folder
	var bg_file_names : Array = Utils.list_files_in_directory(BG_DIR)
	# Note: you can't list files in a directory on release builds for some stupid fudging reason.
	if OS.is_debug_build() and System.build_cache:
		System.directory_contents_cache["bg_file_names"] = bg_file_names
	else:
		bg_file_names = System.directory_contents_cache["bg_file_names"]

	for file_name in bg_file_names:
		if file_name.ends_with("import"):
			continue
		var img_resource : Texture = load(BG_DIR + file_name)
		bg_dict[file_name] = img_resource
	
	texture = bg_dict["black.png"]
	update_background()
	DialogueManager.connect("dialogue_change", self, "update_background")
	
func _process(delta):
	if previous_bg.modulate.a > 0:
		previous_bg.modulate.a -= bg_fade_spd * delta

func update_background():
	var data : Dictionary = DialogueManager.get_current_extra_data()
	
	if !data.has("Background"):
		return
	var bg_name : String = data["Background"].strip_edges()
	
	if bg_name == "":
		return
	
	
	# Get custom fade speed
	bg_fade_spd = default_bg_fade_spd
	var extra_data : Dictionary = DialogueManager.get_current_extra_data()
	if extra_data.has("Fade In Speed"):
		var value = extra_data["Fade In Speed"]
		bg_fade_spd = float(value)
	
	previous_bg.texture = texture
	previous_bg.modulate.a = 1
	
	if bg_dict.has(bg_name):
		texture = bg_dict[bg_name]
	else:
		push_warning("ERROR: Background " + bg_name + "does not exist.")
	
