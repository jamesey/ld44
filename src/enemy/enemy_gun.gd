extends Node2D

onready var bullet : PackedScene = preload("res://src/weapons/bullet.tscn")

export var bullet_spd := 600.0
var recoil := 0.0
export var rumble_power := 0.0
export var shake_screen_power := 0.0


export var shoot_cooldown_timer_time := 0.5
var shoot_cooldown_timer := 0.0

func _physics_process(delta):
	if shoot_cooldown_timer >= 0:
		shoot_cooldown_timer -= delta
	
	if shoot_cooldown_timer <= 0:
		if owner.player_ref != null:
			shoot()
		shoot_cooldown_timer = shoot_cooldown_timer_time

func shoot():
	var new_bullet := bullet.instance()
	new_bullet.global_position = global_position
	new_bullet.is_template = false
	new_bullet.velocity = bullet_spd * Vector2.RIGHT.rotated(owner.rotation)
	new_bullet.ignore_list.append(owner)
	new_bullet.owning_body = owner
	owner.owner.add_child(new_bullet)
	
	if owner is Player or owner is Enemy:
		var recoil_dir : Vector2 = (owner.global_position - new_bullet.global_position).normalized()
		owner.apply_simple_impulse(recoil * recoil_dir)
		
		if shake_screen_power > 0.001:
			GameManager.player_camera.shake_screen(shake_screen_power, 0.25, -new_bullet.velocity.normalized())
	$gunshot.play()
	
	if owner is Player:
		if !owner.keyboard_control:
			Input.start_joy_vibration(0, rumble_power, rumble_power, 0.25)
	
