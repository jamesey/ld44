class_name Enemy
extends KinematicBody2D

var death_explosion : PackedScene= preload("res://fx/Explosion.tscn")

const LEFT_STICK_AXIS_DEADZONE := 0.1
const LEFT_STICK_DEADZONE := 0.4
const LEFT_STICK_FULLZONE := 0.95

onready var vision_cone_area : Area2D = $VisionConeHolder/VisionConeArea
onready var visibility_notifier : VisibilityNotifier2D = $VisibilityNotifier2D

export var health := 3.0

# Tweakers
export var acceleration_spd := 0.35
export var max_velocity := 1.7
export var max_spd_lerp_ramp := 0.1
export var release_damping := 0.05
export var lerp_to_player_spd := 0.01
export var rot_lerp_spd := 0.05
export var lose_sight_distance := 360.0
export var input_vector_turbulence := Vector2(0.5,0.5)
export var attack_other_enemies_if_hit := false
export var orbit_player := false
export var follow_distance := 10000

export var always_aggro := false

var gravity_vector := 0 * Vector2.DOWN # Used for either platformers or "wind" effects
var wall_slide_friction := 0.82 # Used primarily for top down games - set to 1.0 to disable

var impulse_max_velocity_offset_damping := 0.2 # Should be close to acceleration speed

var input_vector := Vector2()
var input_magnitude := 0.0

var velocity := Vector2()

var max_spd_mod := 1.0
var rot_lerp_spd_mod := 1.0

var impulse_max_velocity_offset := Vector2()

var player_ref : KinematicBody2D = null
var distance_to_player_vec := Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	# Have some variation in speed and velocity
	acceleration_spd *= rand_range(0.8, 1.1)
	max_velocity *= rand_range(0.9, 1.1)
	rot_lerp_spd *= rand_range(0.8, 1.2)
	
	if name != "Boss":
		health *= rand_range(0.8, 1.3)
	
	if always_aggro:
		player_ref = GameManager.player
		yield(get_tree().create_timer(0), "timeout")
		player_ref = GameManager.player
	

func _physics_process(delta):	
	if !is_instance_valid(player_ref):
		player_ref = null
	
	if always_aggro:
		player_ref = GameManager.player
	
	if health <= 0:
		var explosion := death_explosion.instance()
		explosion.global_position = global_position
		owner.add_child(explosion)
		GameManager.player.money += GameManager.player.cost["kills"]
		queue_free()
	
	if !visibility_notifier.is_on_screen() and player_ref == null:
		visible = false
		return
		
	visible = true
	
	rot_lerp_spd_mod = rand_range(0.9, 5)
	if player_ref != null:
		if player_ref.velocity.length() <= 0.1:
			rot_lerp_spd_mod = rand_range(1.0, 5)

	
	if player_ref == null:	
		input_vector *= rand_range(0.6, 1)
	else:
		var raw_distance_vec := player_ref.position - position
		if raw_distance_vec.length_squared() <= follow_distance:
			input_vector *= -0.1
		if !orbit_player and raw_distance_vec.length_squared() < 2000:
			distance_to_player_vec = raw_distance_vec
		else:
			distance_to_player_vec = distance_to_player_vec.linear_interpolate(raw_distance_vec, lerp_to_player_spd)

		input_vector = distance_to_player_vec
		input_vector += input_vector_turbulence * rand_range(-1, 1)
		var current_rot_vector := Vector2.RIGHT.rotated(rotation).normalized()
		var new_rot_vector := current_rot_vector.linear_interpolate(raw_distance_vec.normalized(), rot_lerp_spd * rot_lerp_spd_mod * delta * 120)
		rotation = new_rot_vector.angle()
		


	if abs(input_vector.x) < LEFT_STICK_AXIS_DEADZONE:
		input_vector.x = 0
	if abs(input_vector.y) < LEFT_STICK_AXIS_DEADZONE:
		input_vector.y = 0
	input_magnitude = clamp(input_vector.length_squared(), 0, 1)
	
	if input_magnitude > LEFT_STICK_FULLZONE:
		input_magnitude = 1
	elif input_magnitude < LEFT_STICK_DEADZONE * LEFT_STICK_DEADZONE:
		input_magnitude = 0
		
	input_vector = input_magnitude * input_vector.normalized()
	
	var max_spd_mod_target = clamp(input_magnitude, 0.1, 1.0)
	var spd_mod_lerp_spd := 0.05
	# Lerp faster if increasing
	if max_spd_mod_target > max_spd_mod:
		spd_mod_lerp_spd = max_spd_lerp_ramp
	
	max_spd_mod = lerp(max_spd_mod, max_spd_mod_target, spd_mod_lerp_spd)
	if max_spd_mod > 0.95 and max_spd_mod_target > max_spd_mod:
		max_spd_mod = 1
	
	
	var acceleration := acceleration_spd * input_vector
	velocity += input_magnitude * acceleration
	
	# Clamp velocity to never exceed limit 
	var velocity_magnitude : float = clamp(velocity.length(), 0, max_spd_mod * max_velocity + impulse_max_velocity_offset.length())

	# Damp velocity on release (zero input vector)
	if input_magnitude <= 0:
		velocity_magnitude -= release_damping 
		if velocity_magnitude < 0:
			velocity_magnitude = 0
	
	# Damp impulse offset
	var impulse_max_velocity_offset_magnitude := impulse_max_velocity_offset.length()
	impulse_max_velocity_offset_magnitude -= impulse_max_velocity_offset_damping
	if impulse_max_velocity_offset_magnitude < 0:
		impulse_max_velocity_offset_magnitude = 0
	impulse_max_velocity_offset = impulse_max_velocity_offset_magnitude * impulse_max_velocity_offset.normalized()
	
	velocity = velocity_magnitude * velocity.normalized()
	
	# Dash move
	if false:
		var dash_dir := Vector2.ZERO
		if input_vector.length() > 0:
			dash_dir = input_vector.normalized()
		elif velocity.length() > 0:
			dash_dir = velocity.normalized()
		apply_simple_impulse(6 * dash_dir)
		# TODO : use facing direction as last resort
	
	# Apply gravity
	velocity += gravity_vector
	
	# Finally move and check for collisions
	var collision := move_and_collide(velocity * delta * 120)
	# Slide along collision normal if colliding
	if collision:
		velocity = wall_slide_friction * velocity.slide(collision.normal)
		# Clamp velocity again to be safe
		velocity = velocity.clamped(max_velocity + impulse_max_velocity_offset.length())
		move_and_collide(velocity * delta * 120)
	
	check_for_player()

func check_for_player():
	vision_cone_area.scale = Vector2(1.0, 1.0)
	if player_ref != null:
		vision_cone_area.scale = Vector2(1.8, 1.8)
		var player_found := false
		for item in vision_cone_area.get_overlapping_bodies():
			if item is Player:
				return
		# Second check for player if intelligent enough

		if distance_to_player_vec.length() < lose_sight_distance:
			return
		
		player_ref = null
		return
	for item in vision_cone_area.get_overlapping_bodies():
		if item is Player:
			player_ref = item
			return


# Applies a simple impulse to the character.
func apply_simple_impulse(impulse_vector : Vector2, clear_current_velocity := true, break_velocity_limit := true):
	if clear_current_velocity:
		velocity = Vector2()
	
	velocity += impulse_vector
	
	impulse_max_velocity_offset += impulse_vector
	
func damage(amount : float):
	health -= amount